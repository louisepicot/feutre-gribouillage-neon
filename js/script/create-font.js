//
opentype.load(
  "./assets/fonts/ISOBARE/Isobare-Regular.woff",
  function (err, font) {
    if (err) {
      alert("Could not load font: " + err);
    } else {
      let pathSvg,
        glyphArray = [];
      const svg = document.getElementById("svg");

      let glyphsList = font.glyphs.glyphs;

      document.querySelector("#titre").addEventListener("mouseover", (e) => {
        setTimeout(() => {
          for (const property in glyphsList) {
            glyphsList[property].path.commands.forEach((path) => {
              // path.x += 5;
              // path.y += 5;
              if (path.type === "C") {
                console.log(path);
                path.x += 1;
                path.y += 1;
                path.x1 += 1;
                path.y1 += 2;
                path.x2 -= 1;
                path.y2 -= 2;
              } else if (path.type === "L") {
                path.x += 1;
                path.y += 1;
              } else {
                path.x -= 1;
                path.y -= 1;
              }
            });
            glyphArray.push(glyphsList[property]);
            let pathText = font.getPath(`Louise Picot`, 0, 80, 100);
            pathText.fill = "rgb(255, 255, 255)";
            pathSvg = pathText.toSVG();
            svg.innerHTML = " ";
            svg.insertAdjacentHTML("afterbegin", pathSvg);
          }
        }, 100);
      });
    }
  }
);
